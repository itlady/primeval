using System.Collections;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    // this script is attached to each enemy trigger point 

    [Header("Enemy prefab object:")]
    [SerializeField] private GameObject enemyPrefab;
    [Header("Where should enemy appears:")]
    [SerializeField] private GameObject location;

    private GameObject instEnemy;

    // how long the enemy is displayed
    private float timeToLive;

    // default direction on movement
    public bool goRight;
   
    private IEnumerator coroutine;
    public static EnemyGenerator Instance { get; private set; }

    /// <summary>
    /// randomize the length of life
    /// </summary>
    private void Start()
    {
        Instance = this;
        timeToLive = Random.Range(6f, 12f);
        coroutine = Generate();
    }

    /// <summary>
    /// enemy is destroyed after specific time
    /// </summary>
    private void Update()
    {
        Destroy(instEnemy, timeToLive);
    }

    /// <summary>
    /// if the player triggers the point the enemy is generated on specific location, coroutine is used
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Caveman"))
        {
            StartCoroutine(coroutine);
        }
    }

    /// <summary>
    /// the enemy is instantiated from prefab at specific the location based on set direction 
    /// </summary>
    private IEnumerator Generate()
    {
        if (goRight)
        {
            InstantiateEnemy(goRight);
        } else
        {
            InstantiateEnemy(!goRight);
        }
        yield return new WaitForSeconds(5f);
    }

    /// <summary>
    /// enemy is instantiated from prefabs at specific location,
    /// the name of instantiated enemy is changed - 'Clone' part has to be removed,
    /// the value flipX in Sprite Renderer Compoment has to changed according to the enemy direction
    /// </summary>
    /// <param name="direction">direction</param>
    private void InstantiateEnemy(bool direction)
    {
        instEnemy = (GameObject)Instantiate(enemyPrefab, location.transform.position, Quaternion.identity);
        instEnemy.name = instEnemy.name.Replace("(Clone)", "");
        instEnemy.GetComponent<SpriteRenderer>().flipX = !direction;
        instEnemy.GetComponent<SpriteRenderer>().sortingOrder = 10;
    }
}
