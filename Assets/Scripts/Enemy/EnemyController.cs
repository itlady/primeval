using UnityEngine;

public class EnemyController : MonoBehaviour
{
    // this script is attached to enemy prefab object

    [Header("Collider Component:")]
    [SerializeField] private PolygonCollider2D polygon;
    [Header("Collider Component on the top:")]
    [SerializeField] private EdgeCollider2D topCollider;
    
    private Rigidbody2D rb;
    private Animator anim;
    private Vector2 velocity;

    // speed of the enemy movement
    private float speed;

    // direction of the enemy, default is right
    private bool facingRight = true;

    private Vector2[] right;
    private Vector2[] left;
    private Vector2[] rightT;
    private Vector2[] leftT;

    public static EnemyController Instance { get; private set; }

    /// <summary>
    /// randomize the speed of the enemy
    /// </summary>
    private void Awake()
    {
       speed = Random.Range(2f, 4.5f);
    }
    
    /// <summary>
    /// direction at the start is left
    /// </summary>
    private void Start()
    {
        Instance = this;
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.left * speed;
        anim = GetComponent<Animator>();
    }


    private void Update()
    {
        Ignore();
        
    }

    /// <summary>
    /// move and animate the character
    /// </summary>
    private void FixedUpdate()
    {
        rb.velocity = rb.velocity.normalized * speed;
        velocity = rb.velocity;
        anim.SetBool("isMoving", true);
        FlipCharacter();
    }

    /// <summary>
    /// flipping the character based on the direction
    /// </summary>
    private void FlipCharacter()
    {
        if (facingRight && velocity.x < 0 || !facingRight && velocity.x > 0)
        {
            facingRight = !facingRight;
            FlipColliders(facingRight);
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
    }

    /// <summary>
    /// flipping both colliders with the character
    /// </summary>
    /// <param name="direction">direction</param>
    private void FlipColliders(bool direction)
    {
        left = polygon.points;
        right = polygon.points;
        leftT = topCollider.points;
        rightT = topCollider.points;   

        for (int i = 0; i < left.Length; i++)
        {
            left[i].x = -left[i].x;
        }

        for (int i = 0; i < leftT.Length; i++)
        {
            leftT[i].x = -leftT[i].x;
        }

        if (direction == facingRight)
        {
            polygon.points = right;
            topCollider.points = rightT;
        }
        else
        {
            polygon.points = left;
            topCollider.points = leftT;
        }
    }

    /// <summary>
    /// if character collides with boundaries it turns 
    /// </summary>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Stop"))
        {
            FlipCharacter();
            rb.velocity = Vector2.Reflect(rb.velocity, collision.contacts[0].normal);
        }
    }

    /// <summary>
    /// ignore enemies collision with each other, with collectable objects and their colliders 
    /// </summary>
    private void Ignore()
    {
        Physics2D.IgnoreLayerCollision(11, 11);

    }

}
