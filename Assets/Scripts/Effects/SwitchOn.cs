using UnityEngine;

public class SwitchOn : MonoBehaviour
{
    // this script is attached to Switcher Object

    public bool switchOn;
    public static SwitchOn Instance { get; private set; }

    private Animator anim;

    /// <summary>
    /// default state is set to switch off
    /// </summary>
    private void Start()
    {
        Instance = this;
        anim = GetComponent<Animator>();
        anim.SetTrigger("switchOff");
    }

    /// <summary>
    /// if the player's weapon triggers the collider, the state is changed to switch on
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Weapon"))
        {
            anim.SetTrigger("switchOn");
            switchOn = true;
        }
    }

}
