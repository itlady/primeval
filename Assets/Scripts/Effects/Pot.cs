using UnityEngine;

public class Pot : MonoBehaviour
{
    // this script is attached to the Pot object in UI PanelFinishGame

    /// <summary>
    /// make objects disappear after collision with the pot object
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Summary"))
        {
            collision.gameObject.SetActive(false);
        }
    }
}
