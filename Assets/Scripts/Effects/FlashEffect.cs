using System.Collections;
using UnityEngine;

public class FlashEffect : MonoBehaviour
{
    // this script is attached to the player (caveman)

    [SerializeField] private Material flashMaterial;
    private SpriteRenderer sr;
    private Material originalMaterial;
    private Coroutine flashRoutine;

    // how long the effect will take
    private float duration = 0.125f;

    public static FlashEffect Instance { get; private set; }

    private void Start()
    {
        Instance = this;
        sr = GetComponent<SpriteRenderer>();
        originalMaterial = sr.material;
    }

    /// <summary>
    /// start to switch original and flash material, for timing coroutine is used
    /// </summary>
    public void Flash()
    {
        // Check if the flashRoutine is running
        if (flashRoutine != null)
        {
            // Stop running FlashRoutine
            StopCoroutine(flashRoutine);
        }
        // Start Coroutine, make reference for it
        flashRoutine = StartCoroutine(FlashRoutine());
    }

    private IEnumerator FlashRoutine()
    {
        // Switch to flash material
        sr.material = flashMaterial;
        yield return new WaitForSeconds(duration);
        // Switch to original material
        sr.material = originalMaterial;
        // coroutine is finished
        flashRoutine = null;
    }
}
