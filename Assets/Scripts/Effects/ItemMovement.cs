using UnityEngine;

public class ItemMovement : MonoBehaviour
{
    // this script is attached to each item we want to be generated from prefab
    // except those static items inserted directly from the start of the game

    private Rigidbody2D rb;

    // speed of movement
    private float speed = 3.5f;

    /// <summary>
    /// make items move to left or right based on the value set in GenerateCollection script
    /// </summary>
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        if (GenerateCollection.Instance.left)
        {
            rb.velocity = new Vector2(-1f, 1.5f) * speed;
        } else
        {
            rb.velocity = new Vector2(1f, 1.5f) * speed;
        }
    }


}
