using System.Collections;
using UnityEngine;

public class SmallBoxController : MonoBehaviour
{
    // this script is attached to small box that makes up the ground

    private Transform trans;

    // the small box offset along Y-axis
    private float deltaY = 0.2f;

    private Vector2 defaultPos;

    [Header("Object movement option:")]
    [SerializeField] bool canFall = false;
  
    private void Start()
    {
        trans = GetComponent<Transform>();
        defaultPos = trans.position;
    }

    /// <summary>
    /// shifting the box when the character collides with the box
    /// if the box should fall after collision - start changing position, coroutine is used
    /// </summary>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Caveman"))
        {
            trans.position = new Vector2(trans.position.x, trans.position.y - deltaY * Time.deltaTime/0.15f);
        }
        if (collision.gameObject.CompareTag("Caveman") && canFall)
        {
            StartCoroutine(Fall());
        }
    }

    /// <summary>
    /// reverse the position of the box after player steps off, coroutine is used
    /// </summary>
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Caveman") && !canFall)
        {
            StartCoroutine(Wait());
        }
    }
    
    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.5f);
        trans.position = defaultPos;
    }
    
        
    private IEnumerator Fall()
    {
        yield return new WaitForSeconds(0.7f);
        trans.position = new Vector2(0,-1f * Time.deltaTime);
        yield return new WaitForSeconds(4f);
        trans.position = defaultPos;
    }

    

}









