using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Semaphore : MonoBehaviour
{
    // this script is attached to the Semaphore object
    
    // semaphore cannot be used
    public bool allow = false;

    // level is complete
    public bool isEnd = false;

    private Animator anim;

    public static Semaphore Instance { get; private set; }


    void Start()
    {
        Instance = this;
        anim = GetComponent<Animator>();
        anim.SetTrigger("deny");
    }
    
    /// <summary>
    /// animate Semaphore based on the value passed from the SwitchOn script
    /// </summary>
    private void Update()
    {
        if (SwitchOn.Instance.switchOn)
        {
            anim.SetTrigger("allow");
            allow = true;
        }        
    }

    /// <summary>
    /// player enter to the trigger collider, the switcher has to be on 
    /// value passed from the SwitchOn script
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Caveman") && allow)
        {
            isEnd = true;
        } 
    }
}
