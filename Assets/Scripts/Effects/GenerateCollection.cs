using System.Collections.Generic;
using UnityEngine;

public class GenerateCollection : MonoBehaviour
{
    // script is attached to Triggers of Collections

    [Header("List of generated items:")]
    public List<GameObject> items = new List<GameObject>();

    [Header("Where items are generated from:")]
    [SerializeField] private GameObject startLocation;

    [Header("Smoke Options:")]
    [SerializeField] private GameObject smokeLocation;
    [SerializeField] private GameObject smokePrefab;
    
    [Header("Default direction of items:")]
    public bool left;
    
    private GameObject instSmoke;
    private GameObject instItem;

    // time between new item is generated
    private float delay = 2f;
    
    private float currentTime;

    // how long the smoke is displayed
    private float smokeTime = 0.3f;

    // how long the objects is displayed
    private float timeToLive = 10f;

    // how many items is in collection
    private int count;

    private bool isReady;

    public static GenerateCollection Instance { get; private set; }
    
    private void Start()
    {
        Instance = this;
        count = items.Count;
    }

    private void Update()
    {
        if (currentTime > 0)
        {
            currentTime -= Time.deltaTime;
        }
        else
        {
            if (isReady)
            {
                Invoke("GenerateItem", 0.2f);
                currentTime = delay;
                Destroy(instItem, 10f);
            }
        }
    }

    /// <summary>
    /// the player enters to the trigger collider and the attacking key is pressed at the same time
    /// then the smoke is generated
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Weapon") && (PlayerAttack.Instance.attacking))
        {
            isReady = true;
            GenerateSmoke();
        }
        else
        {
            isReady = false;
        }
    }

    /// <summary>
    /// items are generated in random order, are instantiated from prefabs at specific location,
    /// the name of instantiated item is changed - 'Clone' part has to be removed,
    /// used in Update function in Invoke method
    /// </summary>
    private void GenerateItem()
    {
        int index = Random.Range(0, items.Count);
        if (items.Count > 0)
        {
            instItem = (GameObject)Instantiate(items[index], startLocation.transform.position, Quaternion.identity);
            instItem.name = instItem.name.Replace("(Clone)", "");
            items.RemoveAt(index);
        }
    }

    /// <summary>
    /// the smoke is instantiated from prefab at specific the location and destroy after certain time
    /// </summary>
    private void GenerateSmoke()
    {
        if (count != 0)
        {
            instSmoke = (GameObject)Instantiate(smokePrefab, smokeLocation.transform.position, Quaternion.identity);
            isReady = true;
            Destroy(instSmoke, smokeTime);
        }
    }
       
    // TODO: make items flash before they disappear
    //private IEnumerator Countdown()
    //{
    //    int i = 5;
    //    yield return new WaitForSeconds(timeToLive);
    //    if (instItem != null)
    //    {
    //        while (i > 0)
    //        {
    //            //FlashEffect.Instance.Flash();
    //            yield return new WaitForSeconds(0.3f);
    //            i--;
    //        }
    //        Destroy(instItem, 0.3f);
    //    }
    //}




}
