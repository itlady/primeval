using UnityEngine;

public class FinishBlocksController : MonoBehaviour
{
    // this script is attached to the FinishGround object

    [Header("Colliders of finish block:")]
    [SerializeField] private BoxCollider2D[] blocks;

        
    /// <summary>
    /// if the player triggers the finish ground box then he can go through the ground upwards
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Caveman"))
        {
            DisableColliders();
        }
    }

    /// <summary>
    /// if the player exits the trigger he cannot go through the ground downwards
    /// </summary>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Caveman"))
        {
            EnableColliders();
        }
    }

    ///// <summary>
    ///// turn off colliders of finish block
    ///// </summary>
    private void DisableColliders()
    {
        foreach (var block in blocks) 
        {
            block.enabled = false;
        }
    }

    ///// <summary>
    ///// turn on colliders of finish block
    ///// </summary>
    private void EnableColliders()
    {
        foreach (var block in blocks)
        {
            block.enabled = true;
        }
    }
}
