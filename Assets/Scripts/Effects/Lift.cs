using UnityEngine;

public class Lift : MonoBehaviour
{
    // this script is attached to the lift object

    [Header("Where the lift starts:")]
    [SerializeField] Transform pointStart;
    [Header("Where the lift goes to:")]
    [SerializeField] Transform pointEnd;

    private float speed = 3f;
    private bool canLift;


    /// <summary>
    /// set ignoring layer at the start
    /// </summary>
    private void Start()
    {
        transform.position = pointStart.position;
        Physics2D.IgnoreLayerCollision(6,15);
    }

    /// <summary>
    /// lift movement from the start point to the end point, player is the trigger,
    /// Vector2.Distance checks the position of the lift if it is close to the end point
    /// </summary>
    private void Update()
    {
        if (Vector2.Distance(transform.position, pointEnd.position) < 0.01f)
        {
            canLift = false;
        }

        if (canLift)
        {
            transform.position = Vector2.MoveTowards(transform.position, pointEnd.position, speed * Time.deltaTime);
        }
        else
        {
            transform.position = Vector2.MoveTowards(transform.position, pointStart.position, speed * Time.deltaTime);
        }
    }

    /// <summary>
    /// return true if player collides with the lift object 
    /// </summary>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Caveman"))
        {
            canLift = true;
        }
    }
}
