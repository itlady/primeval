using UnityEngine;

public class GenerateIsland : MonoBehaviour
{
    // this script is attached to Triggers of Hidden Islands

    [Header("Island Object Prefab:")]
    [SerializeField] private GameObject islandPrefab;

    [Header("Where island will be displayed:")]
    [SerializeField] private GameObject startLocation;

    [Header("Smoke Options:")]
    [SerializeField] private GameObject smokeLocation;
    [SerializeField] private GameObject smokePrefab;

    private GameObject instSmoke;
    private GameObject instIsland;

    private bool islandIsDone = false;
    
    public static GenerateIsland Instance { get; private set; }

    private void Start()
    {
        Instance = this;
    }

    /// <summary>
    /// player enter to the trigger collider and the attacking key is pressed at the same time
    /// then the smoke is generated
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Weapon") && (PlayerAttack.Instance.attacking))
        {   
            if (!islandIsDone)
            {
                GenerateSmoke();
            }
        }
    }

    /// <summary>
    /// the island is instantiated from prefabs at start location,
    /// used in Invoke method after smoke is generated
    /// </summary>
    private void GenerateItem()
    {
        instIsland = (GameObject)Instantiate(islandPrefab, startLocation.transform.position, Quaternion.identity);
        islandIsDone = true;
    }

    /// <summary>
    /// the smoke is instantiated from prefab at specific the location and destroy after certain time
    /// </summary>
    private void GenerateSmoke()
    {
        instSmoke = (GameObject)Instantiate(smokePrefab, smokeLocation.transform.position, Quaternion.identity);
        Destroy(instSmoke, 0.3f);
        Invoke("GenerateItem", 0.5f);
    }
}
