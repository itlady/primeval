using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class Summary : MonoBehaviour
{
    // this script is attached to the GameManager object

    [SerializeField] private Transform SummaryPanel;
    [SerializeField] private GameObject[] items;
    private float delay = 1f;
      
    public static Summary Instance { get; private set; }

    private void Start()
    {
        Instance = this;
    }

    public void SummaryItems()
    {
        if (ScorePointsHolder.collected != null)
        {
            StartCoroutine(SpreadItems());
        }
    }

    private IEnumerator SpreadItems()
    {
        yield return new WaitForSeconds(delay);
    }
}