using UnityEngine;

public class AudioController : MonoBehaviour
{
    // this script is attached to AudioController GameObject

    [Header("Sounds played when switching states:")]
    [SerializeField] private AudioSource introSound;
    [SerializeField] private AudioSource themeSound;
    [SerializeField] private AudioSource gameOverSound;
    [SerializeField] private AudioSource finishSound;

    [Header("Sounds played bacharacters behaviour:")]
    [SerializeField] private AudioSource jumpSound;
    [SerializeField] private AudioSource hitSound;
    [SerializeField] private AudioSource gulpSound;
    [SerializeField] private AudioSource whySound;
    [SerializeField] private AudioSource noSound;

    public static AudioController Instance { get; set; }

    private void Start()
    {
        Instance = this;
    }


    public void StopIntro()
    {
        introSound.Stop();
    }

    public void PlayThemeSound()
    {
        themeSound.Play();
    }

    public void StopThemeSound()
    {
        themeSound.Stop();
    }

    public void PlayGameOverSound()
    {
       gameOverSound.Play();
    }

    public void StopGameOverSound()
    {
        gameOverSound.Stop();
    }

    public void PlayFinishSound()
    {
        finishSound.Play();
    }

    public void StopFinishSound()
    {
        finishSound.Stop();
    }

    public void PlayJumpSound()
    {
        jumpSound.Play();
    }

    public void PlayHitSound()
    {
        hitSound.Play();
    }

    public void PlayGulpSound()
    {
        gulpSound.Play();
    }

    
    public void PlayWhySound()
    {
        whySound.Play();
    }

    public void PlayNoSound()
    {
        noSound.Play();
    }


}
