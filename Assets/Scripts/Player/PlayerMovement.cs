using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // this script is attached to the player (caveman)

    [Header("Movement options:")]
    [SerializeField] private float speed;
    [SerializeField] private float jumpPower;

    [Header("Ground check options:")]
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;

    //[Header("Enemy's head check options:")]
    //[SerializeField] private LayerMask headLayer;

    [Header("Player's body collider:")]
    [SerializeField] private PolygonCollider2D polygon;

    [Header("Options changed by script:")]
    public bool onHead;
    public bool isGameOver = false;
    public bool facingRight = true;

    private float horizontal;
    private bool doubleJump;

    private Vector2[] right;
    private Vector2[] left;

    private Rigidbody2D rb;
    private Animator anim;

    public static PlayerMovement Instance { get; private set; }

    private void Start()
    {
        Instance = this;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        
    }

    /// <summary>
    /// set movement as horizontal - use LeftArrow/RightArrow (alternatively A/D)
    /// </summary>
    private void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        Move();
        Jump();
        Flip();
        Ignore();
    }

    /// <summary>
    /// player movement in horizontal way
    /// </summary>
    private void FixedUpdate()
    {
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
    }

    /// <summary>
    /// animating player movement
    /// </summary>
    private void Move()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            anim.SetBool("isMoving", true);
        } else
        {
            anim.SetBool("isMoving", false);
        }
    }

    /// <summary>
    /// jump / double jump  based on Space key pressed
    /// controls when the player touches the ground
    /// </summary>
    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (IsGrounded() || doubleJump)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpPower);
                doubleJump = !doubleJump;
                anim.SetTrigger("Jump");
                AudioController.Instance.PlayJumpSound();
            }
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && rb.velocity.y > 0)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
            anim.SetTrigger("Jump");
        }
    }

    /// <summary>
    /// flipping the player based on the direction
    /// </summary>
    public void Flip()
    {
        if (facingRight && horizontal < 0 || !facingRight && horizontal > 0)
        {
            facingRight = !facingRight;
            FlipBodyCollider(facingRight);
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
    }

    /// <summary>
    /// flipping the collider with the player
    /// </summary>
    /// <param name="direction">direction</param>
    private void FlipBodyCollider(bool direction)
    {
        left = polygon.points;
        right = polygon.points;

        for (int i = 0; i < left.Length; i++)
        {
            left[i].x = -left[i].x;
        }

        if (direction == facingRight)
        {
            polygon.points = right;
        }
        else
        {
            polygon.points = left;
        }
    }

    /// <summary>
    /// check if the player touches the ground
    /// </summary>
    private bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
    }
    
    /// <summary>
    /// make player ignore objects in Ignore Layer
    /// </summary>
    private void Ignore() 
    { 
        Physics2D.IgnoreLayerCollision(10,15);
        Physics2D.IgnoreLayerCollision(10, 17);

    }


    /// <summary>
    /// if the player exits the scene if cause game over
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("GameOver"))
        {
            isGameOver = true;
            GameOverMove();
            AudioController.Instance.PlayNoSound();
        }
    }

    /// <summary>
    /// checks if the player is standing on enemy's head
    /// </summary>
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("HeadCheck"))
        {
            onHead = true;
        }
        else
        {
            onHead = false;
        }
    }

    /// <summary>
    /// game over behavior
    /// </summary>
    public void GameOverMove()
    {
        int gmSpeed = 20;
        if (facingRight)
        {
            rb.velocity = new Vector2(1,1f) * gmSpeed;
        } else 
        { 
            rb.velocity = new Vector2(-1,1f) * gmSpeed;
        }
        anim.SetTrigger("FallDown");
    }
}
