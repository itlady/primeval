using System.Collections;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    // this script is attached to the player (caveman)

    [Header("Player's colliders on weapon:")]
    [SerializeField] private CircleCollider2D rightColl;
    [SerializeField] private CircleCollider2D leftColl;

    [Header("Options changed by script:")]
    public bool attacking = false;
    public bool takeDamage = false;
    
    // key must correspond with the name of the collision object
    private string key;

    private Animator anim;

    public static PlayerAttack Instance { get; private set; }

    private void Start()
    {
        Instance = this;
        anim = GetComponent<Animator>();
        rightColl.enabled = false;
        leftColl.enabled = false;
    }

    private void Update()
    {
        Attack();
    }

    /// <summary>
    /// attack action after space key pressed
    /// </summary>
    private void Attack()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetTrigger("Attack");
            AudioController.Instance.PlayHitSound();
        }
    }

    /// <summary>
    /// start of the attack, animation event is used
    /// </summary>
    public void AttackStart()
    {
        ShowCollider(PlayerMovement.Instance.facingRight);
        attacking = true;
    }


    /// <summary>
    /// end of the attack, animation event is used
    /// </summary>
    public void AttackEnd()
    {
        HideCollider();
        attacking = false;
    }


    /// <summary>
    /// the player collides with the enemy and the attack key is pressed at the same time
    /// considering if the player stands on the enemy's head/back
    /// if attack is succesfull enemy disappears and score point is added
    /// if not the heart count is reduced and flasheffect takes places
    /// </summary>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        takeDamage = false;
        if (collision.gameObject.CompareTag("Enemy"))
        {
            key = collision.gameObject.name;
            
            if (!PlayerMovement.Instance.onHead)
            {
                if (attacking)
                {
                    StartCoroutine(Hit());
                    collision.gameObject.SetActive(false);
                }
                else
                {
                    GetDamage();
                }
            } else
            {
                if (attacking)
                {
                    StartCoroutine(Hit());
                    collision.gameObject.SetActive(false);
                } else
                {
                    return;
                }
            }
        }
    }

    /// <summary>
    /// action after hit the enemy
    /// </summary>
    private IEnumerator Hit()
    {
        GameManager.Instance.Score += ScorePointsHolder.scorePoints[key];
        AudioController.Instance.PlayGulpSound();
        yield return new WaitForSeconds(2f);
    }

    /// <summary>
    /// action after player being damaged
    /// </summary>
    private void GetDamage()
    {
        FlashEffect.Instance.Flash();
        GameManager.Instance.Hearts--;
        AudioController.Instance.PlayWhySound();
        anim.SetTrigger("Damage");
        LivesManager.Instance.DecreasingHearts(GameManager.Instance.Hearts);
    }
      

    /// <summary>
    /// activate the Circle Collider based on player facing
    /// </summary>
    /// <param name="direction">direction</param>
    private void ShowCollider(bool direction)
    {
        if (direction == PlayerMovement.Instance.facingRight)
        {
            rightColl.enabled = true;
            leftColl.enabled = false;
        } else
        {
            leftColl.enabled = true;
            rightColl.enabled = false;
        }
    }

    /// <summary>
    /// deactivate the Circle Collider
    /// </summary>
    /// <param name="direction">direction</param>
    private void HideCollider()
    {
        if (rightColl.enabled)
        {
            rightColl.enabled = !rightColl.enabled;
        }
        if (leftColl.enabled)
        {
            leftColl.enabled = !leftColl.enabled;
        }
    }
}
