using UnityEngine;

public class ScoreCounter : MonoBehaviour
{
    // this script is attached to the player (caveman)
    
    public static ScoreCounter Instance { get; private set; }

    private void Start()
    {
        Instance = this;
    }

    /// <summary>
    /// the player collides with collectable objects, 
    /// score is increasing, it compares the name of the object with the key in dictionary in ScorePointsHolder,
    /// score is added to scoreBoard list,
    /// collectable gameObjects add to List<Gameobjects>
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        string key = collision.gameObject.name;

        if (collision.gameObject.CompareTag("Collect"))
        {
            GameManager.Instance.Score += ScorePointsHolder.scorePoints[key];
            AudioController.Instance.PlayGulpSound();
            ScorePointsHolder.scoreBoard.Add(ScorePointsHolder.scorePoints[key]);
            ScorePointsHolder.collected.Add(collision.gameObject);
            collision.gameObject.SetActive(false);
        } 
        else if (collision.gameObject.CompareTag("Heart"))
        {
            if (GameManager.Instance.Hearts < 5)
            {
                collision.gameObject.SetActive(false);  
                GameManager.Instance.Hearts++;
                LivesManager.Instance.IncreasingHearts(GameManager.Instance.Hearts);
            } else
            {
                collision.gameObject.SetActive(true);
            }
        }
    }


    
}
