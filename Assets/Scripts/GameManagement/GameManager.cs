using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Player options:")]
    [SerializeField] private Transform cavemanStartPosition;

    [Header("UI panel components:")]
    [SerializeField] private GameObject TopPanel;
    [SerializeField] private GameObject BottomPanel;
    [SerializeField] private GameObject PanelFinishGame;
    [SerializeField] private GameObject PanelGameOver;

    [Header("UI text components:")]
    [SerializeField] private TextMeshProUGUI ScoreValue;
    [SerializeField] private TextMeshProUGUI FinalScoreValue;
    [SerializeField] private TextMeshProUGUI PercentValue;

    [Header("Player Object:")]
    [SerializeField] private Transform Player;

    [Header("Player's start position:")]
    [SerializeField] private Transform StartPosition;

    private bool isSwitchingState;

    public static GameManager Instance { get; private set; }

    /// <summary>
    /// score variable declaration
    /// </summary>
    private int score;
    public int Score
    {
        get { return score; }
        set
        {
            score = value;
            ScoreValue.text = score.ToString();
        }
    }

    /// <summary>
    /// hearts variable declaration
    /// </summary>
    private int hearts;
    public int Hearts
    {
        get { return hearts; }
        set
        {
            hearts = value;
        }
    }

    /// <summary>
    /// custom enum type
    /// </summary>
    public enum State
    {
        INIT, PLAY, FINISH, GAMEOVER
    }

    private State state;
           

    /// <summary>
    /// default state is set to INIT
    /// </summary>
    private void Start()
    {
        Instance = this;
        UpdateState(State.INIT);
    }
       
       
    /// <summary>
    /// method that switches states of the game, coroutine is used
    /// </summary>
    /// <param name="delay">length of transition between switching states</param>
    public void UpdateState(State newState, float delay = 0)
    {
        StartCoroutine(SwitchDelay(newState, delay));
    }

    IEnumerator SwitchDelay(State newState, float delay)
    {
        isSwitchingState = true;
        yield return new WaitForSeconds(delay);
        EndState();
        state = newState;
        BeginState(newState);
        isSwitchingState = false;
    }

    /// <summary>
    /// setup if new state is in progress
    /// </summary>
    private void BeginState(State newState)
    {
        switch (newState)
        {
            case State.INIT:
                Cursor.visible = false;

                // reseting values to default
                FinalScoreValue.text = "";
                Score = 0;
                Hearts = 5;
                ScorePointsHolder.scoreBoard.Clear();

                // panels options
                PanelFinishGame.SetActive(false);
                PanelGameOver.SetActive(false);

                // audio options
                AudioController.Instance.StopIntro();
                AudioController.Instance.StopFinishSound();
                AudioController.Instance.StopGameOverSound();
                AudioController.Instance.PlayThemeSound();

                // player options
                ResetPlayerPosition();

                UpdateState(State.PLAY);
                break;

            case State.PLAY:
                break;

            case State.FINISH:
                Cursor.visible = true;

                // transfering values
                PercentValue.text = ScorePointsHolder.Percent().ToString() + " %";
                FinalScoreValue.text = score.ToString();

                //panel options
                PanelFinishGame.SetActive(true);

                // audio options
                AudioController.Instance.StopThemeSound();
                AudioController.Instance.PlayFinishSound();

                // this script was not used
                // Summary.Instance.SummaryItems();
                break;

            case State.GAMEOVER:
                Cursor.visible = true;

                //panel options
                PanelGameOver.SetActive(true);

                //audio options
                AudioController.Instance.StopThemeSound();
                AudioController.Instance.PlayGameOverSound();

                // player options
                PlayerMovement.Instance.GameOverMove();

                break;
        }
    }

    /// <summary>
    /// set Q key as the quit key,
    /// options if current state is in progress
    /// </summary>
    private void Update()
    {
        switch (state)
        {
            case State.INIT:
                break;
            case State.PLAY:
                
                // out of hearts -> gameover
                if (Hearts == 0 || PlayerMovement.Instance.isGameOver)
                {
                    Invoke("GameOver", 2f);
                }

                // player reaches the finish
                if (Semaphore.Instance.isEnd)
                {
                    UpdateState(State.FINISH, 2f);
                }

                break;
            case State.FINISH:
                break;
            case State.GAMEOVER:

                break;
        }

        PressQToQuit();
    }


    /// <summary>
    /// setup if current state ends
    /// </summary>
    private void EndState()
    {
        switch (state)
        {
            case State.INIT:
                break;
            case State.PLAY:
                break;
            case State.FINISH:
                Cursor.visible = false;

                //audio options
                AudioController.Instance.StopFinishSound();
            
                break;
            case State.GAMEOVER:
                Cursor.visible = false;

                // audio options
                AudioController.Instance.StopGameOverSound();

                break;
        }
    }

    /// <summary>
    /// set the position of player to the start point position
    /// </summary>
    private void ResetPlayerPosition()
    {
        Player.position = StartPosition.position;
    }


    /// <summary>
    /// used for Invoke method, when the player has no hearts
    /// </summary>
    private void GameOver() 
    { 
        UpdateState(State.GAMEOVER);
    }

    /// <summary>
    /// allows to quit the game by pressing Q key
    /// </summary>
    private void PressQToQuit()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
           Application.Quit();
        }
    }

     
}
