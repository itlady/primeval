using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ScorePointsHolder
{
    /// <summary>
    /// maximum count of items that can be collected - 100 %,
    /// depends on placements of objects in collection
    /// </summary>
    public static int maximum = 44;

    /// <summary>
    /// list for storing collecting items points
    /// </summary>
    public static List<int> scoreBoard = new List<int>();

    /// <summary>
    /// list for storing gameObjects of collecting items
    /// </summary>
    public static List<GameObject> collected = new List<GameObject>();

    /// <summary>
    /// dictionary holds the name of objects and the corresponding points 
    /// </summary>
    public static Dictionary<string, int> scorePoints = new Dictionary<string, int>()
    {
        { "donut", 25000 },
        { "beer" , 500 },
        { "card1", 2000 },
        { "egg", 300 },
        { "goblet", 2000 },
        { "greenDiamond", 2000 },
        { "strawberry", 1500 },
        { "icecream", 25000 },
        { "candle", 1000 },
        { "drink", 1000 },
        { "gold" , 1000 },
        { "grenade", 2000 },
        { "ring", 500 },
        { "origami", 200 },
        { "banana", 20000 },
        { "grapes", 30000 },
        { "card2", 2000 },
        { "icelolly", 500 },
        { "cheese", 500},
        { "milk", 1000 },
        { "pencil", 1000 },
        { "yellowDiamond", 5000 },
        { "burger", 30000 },
        { "ham", 30000 },
        { "star", 1000 },
        { "fries", 25000 },
        { "cake", 500 },
        { "shoe", 1000 },
        { "floppy", 500 },
        { "flower", 1000 },
        { "smallice", 500 },
        { "enemy", 200 },
    };
    
    /// <summary>
    /// count the percentage of collected objects
    /// </summary>
    /// <returns>percent of collected objects</returns>
    public static int Percent()
    {
        return (int)Math.Round((double)scoreBoard.Count / maximum * 100);
    }
}
