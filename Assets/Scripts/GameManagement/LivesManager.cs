using System.Collections.Generic;
using UnityEngine;

public class LivesManager : MonoBehaviour
{
    // this script is attached to GameManager object

    [Header("List of hearts:")]
    [SerializeField] private List<GameObject> hearts = new List<GameObject>();

    public static LivesManager Instance { get; private set; }

    private void Start()
    {
        Instance = this;
    }

    /// <summary>
    /// decreases the number of hearts
    /// </summary>
    public void DecreasingHearts(int count)
    {
        switch(count)
        {
            case 4:
                hearts[4].SetActive(false);
                break;
            case 3:
                hearts[3].SetActive(false);
                break;
            case 2:
                hearts[2].SetActive(false);
                break;
            case 1:
                hearts[1].SetActive(false);
                break;
            case 0:
                hearts[0].SetActive(false);
                break;
        }
    }


    /// <summary>
    /// increases the number of hearts
    /// </summary>
    /// <param name="count"></param>
    public void IncreasingHearts(int count)
    {
        switch (count)
        {
            case 5:
                hearts[4].SetActive(true);
                break;
            case 4:
                hearts[3].SetActive(true);
                break;
            case 3:
                hearts[2].SetActive(true);
                break;
            case 2:
                hearts[1].SetActive(true);
                break;
        }
    }

    /// <summary>
    /// display all the heart at the beginning
    /// </summary>
    public void ShowHearts()
    {
        foreach (var heart in hearts)
        {
            heart.SetActive(true);
        }
    }
}
