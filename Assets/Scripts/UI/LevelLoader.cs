using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    // this script is attached to LevelLoader object in Menu scene

    [Header("Loading Screen")]
    public GameObject loadingScreen;
    [Header("Slider")]
    public Slider slider;


    /// <summary>
    /// show loading slider between Menu and Game scenes, asynchronous coroutine is used
    /// </summary>
    /// <param name="sceneIndex">sceneIndex</param>
    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    private IEnumerator LoadAsynchronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            yield return null;
        }
    }

    /// <summary>
    /// After click on the button the application quits
    /// </summary>
    public void QuitTheGame()
    {
        Application.Quit();
    }
}
