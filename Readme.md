# <center>PRIMEVAL</center>
Počítačová hra Primeval byla vytvořena v rámci zpracování Absolventské práce na VOŠ na téma **"Jednoduchá počítačová hra v jazyce C# s využitím vhodných knihoven a enginů."**  

&nbsp;

---
## Požadavky pro spuštění
* Hra je zkompilována pro operační systém x64 Windows a MacOS. Doporučené rozlišení je FullHD (1920x1080).

&nbsp;

---

## Ovládání
* Hra se ovládá směrovými šipkami vpravo a vlevo, šipka nahoru slouží pro skok/dvojitý skok. Útok se provádí mezerníkem.

&nbsp;

---
## Použité technologie a nástroje
* herní engine Unity
* programovací jazyk C#, MS Visual Studio
* nástroj pro tvorbu vektorové grafiky Inkscape
* nástroj pro práci s bitmapovou grafikou Gimp
* nástroj pro úpravu zvukové stopy Audacity

&nbsp;

---
## Spuštění hry
* Windows: binární soubor Primeval.exe
* MacOS: MacOS.app

&nbsp;

---
## Inspirace
* volná inspirace podle hry Prehistorik2 - MS-DOS, 1993, Titus Interactive

&nbsp;

---
## Autor
* Bc. Lucie Názrová, Dis.

